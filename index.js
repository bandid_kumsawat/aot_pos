const escpos = require('escpos');
// install escpos-usb adapter module manually
escpos.USB = require('escpos-usb');
// Select the adapter based on your printer type
const device  = new escpos.USB();
// const device  = new escpos.Network('localhost');
//const device  = new escpos.Serial('/dev/usb/lp0');

const options = { encoding: "GB18030" /* default */ }
// encoding is optional

const printer = new escpos.Printer(device, options);

// var getPixels = require("get-pixels")
 
// getPixels("./image3.jpg", function(err, pixels) {
//   if(err) {
//     console.log("Bad image path")
//     return
//   }
//   console.log("got pixels", pixels.shape.slice())
// })

// escpos.Image.load('./image3.jpg', 'image/jpg', function(Image_){
//   device.open(function(error){
//     printer
//       .align('ct')
//       .image(Image_, 'd24')
//       .then(() => { 
//         printer
//           .cut()
//           .close(); 
//      });
//   });
// })
device.open(function(error){
  printer
    .align('lt')
    .font('A')
    .barcode('112255443366', 'EAN13', {
      width: 80,
      height: 100,
      position:0,
      font: 'A'
    })
    .barcode('112255443366', 'EAN13', {
      width: 80,
      height: 100,
      position:0,
      font: 'A'
    })
    .barcode('112255443366', 'EAN13', {
      width: 80,
      height: 100,
      position:0,
      font: 'A'
    })
    // .getStatus('PrinterStatus', function (data) {
    //   console.log(data);
    // })
    // .getStatus('OfflineCauseStatus', function (data) {
    //   console.log(data);
    // })
    // .getStatus('ErrorCauseStatus', function (data) {
    //   console.log(data);
    // })
    .getStatus('RollPaperSensorStatus', function (data) {
      console.log(data);
    })
    .close()
    .qrimage('hello', function(err){
      this.cut()
      this.close();
    })
});

/**
 * PrinterStatus
 * OfflineCauseStatus
 * ErrorCauseStatus
 * RollPaperSensorStatus
 */