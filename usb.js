var usb = require('usb')
  // ,term = usb.findByIds(1305, 8211);
var DevicePrinter = usb.getDeviceList().filter(function(device){
  try{
    return device.configDescriptor.interfaces.filter(function(iface){
      return iface.filter(function(conf){
        return conf.bInterfaceClass === 0x07; // 0x07 = printer
      }).length;
    }).length;
  }catch(e){
    // console.warn(e)
    return false;
  }
});
var vid = DevicePrinter[0].deviceDescriptor.idVendor
var pid = DevicePrinter[0].deviceDescriptor.idProduct

var term = usb.findByIds(vid, pid);

var endpoints = term.interfaces[0].endpoints,
    inEndpoint = endpoints[0],
    outEndpoint = endpoints[1];
console.log(inEndpoint)
console.log(outEndpoint)
// inEndpoint.transferType = 2;
// inEndpoint.startStream(1, 64);
// inEndpoint.transfer(64, function (error, data) {
//     if (!error) {
//         console.log(data);
//     } else {
//         console.log(error);
//     }
// });
// inEndpoint.on('data', function (data) {
//     console.log(data);
// });
// inEndpoint.on('error', function (error) {
//     console.log(error);
// });
// outEndpoint.transferType = 2;
// outEndpoint.startStream(1, 64);
// outEndpoint.transfer(new Buffer('d\n'), function (err) {
//     console.log(err);
// });